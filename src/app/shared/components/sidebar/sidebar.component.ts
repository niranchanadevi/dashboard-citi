import { Component, OnInit } from '@angular/core';

export interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  { path: '/',     title: 'Dashboard',         icon:'assets/img/dashboard.png',       class: '' },
  { path: '/posts',         title: 'HealthCheck',             icon:'assets/img/health.png',    class: '' },
  { path: '/incident',          title: 'Incident Management',              icon:'assets/img/incident.png',      class: '' },
  { path: '/greenzone', title: 'Greenzone Management',     icon:'assets/img/greenzone.png',    class: '' },
  { path: '/changemanagement',          title: 'Change Management',      icon:'assets/img/change.png',  class: '' },
  { path: '/defectmanagement',         title: 'Defect Management',        icon:'assets/img/defect.jpeg',    class: '' },
  { path: '/patch management',    title: 'Patch Management',        icon:'assets/img/patch.png', class: '' },
  { path: '/codedrop',       title: 'Code drop Management',    icon:'assets/img/code.png',  class: '' },
  { path: '/knowledge',       title: 'Knowledge Management',    icon:'assets/img/knowledge.png',  class: '' },
  { path: '/certificate',       title: 'Certificate',    icon:'assets/img/certificate.jpeg',  class: 'active-pro' },

];


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})


export class SidebarComponent implements OnInit {
  public menuItems: any[];
  ngOnInit() {
      this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
}
